export const buttonType = {
    RECTANGLE: 'RECTANGLE',
    CIRCLE: 'CIRCLE',
    CLEAR: 'CLEAR',
}

export const colorType = {
    BLACK: 'BLACK',
    RED: 'RED',
    YELLOW: 'YELLOW',
    GREEN: 'GREEN',
    BLUE: 'BLUE',
}
