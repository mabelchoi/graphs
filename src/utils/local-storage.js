export const getGraphs = () => JSON.parse(localStorage.getItem('GRAPHS'));

export const setGraph = (graph) => { localStorage.setItem('GRAPHS', JSON.stringify(graph)) };

export const clearGraphs = () => localStorage.removeItem('GRAPHS');
