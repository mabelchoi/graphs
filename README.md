# GRAPH TEST

웹 브라우저에서 사용할 수 있는 간단한 도형 그림판으로 프로젝트 인스톨 후 실행 시키면
간단한 사각형 및 원형 도형을 그릴 수 있습니다.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### 사용 언어

* VueJS 3.0
* node (V14.15.1)

### 구현한 기능

* 마우스를 드래그해서 사각형을 그릴 수 있습니다.
* 마우스를 드래그해서 원을 그릴 수 있습니다. 
* 모든 도형을 일괄 삭제할 수 있습니다.
* 그려진 도형은 Client-side storage에 저장되어 페이지를 새로고침 해도 유지되어야 합니다.
* 도형을 선택해서 배경색, 테두리색을 지정할 수 있습니다.

### 미구현 기능

* 마우스를 드래그해서 선을 그릴 수 있습니다.
* 도형을 선택해서 삭제할 수 있습니다.
* 도형을 선택해서 드래그로 위치를 이동할 수 있습니다.
* 도형을 선택해서 드래그로 크기를 조정할 수 있습니다.
* 도형을 선택해서 표시 순서를 바꿀 수 있습니다. (e.g. 앞으로/맨 앞으로 가져오기, 뒤로/맨 뒤로 보내기)

### TODO LIST

* Test Code 추가 필요
* 중복된 코드 공통화
* Color picker 적용
